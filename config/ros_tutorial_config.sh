#!/bin/sh

# ==== Personal config ====
# catkin
CATKIN_WS=catkin_ws
BLACKLIST_PACK=""
# BLACKLIST_PACK="rls_surface_matching;rls_task_planning"


# ====  ROS config ====
source /opt/ros/kinetic/setup.bash
source ~/${CATKIN_WS}/devel/setup.bash

export ROS_WORKSPACE=~/${CATKIN_WS}/

alias cm='cd ~/${CATKIN_WS} && catkin_make'
alias cb='cd ~/${CATKIN_WS} && catkin_make -DCATKIN_BLACKLIST_PACKAGES=${BLACKLIST_PACK}'
alias cs='cd ~/${CATKIN_WS}/src'


